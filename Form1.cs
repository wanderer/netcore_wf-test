﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace netcore_wf_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string a = textBox1.Text;
            if (a == "")
            {
                MessageBox.Show("use the textbox", "you typed nothing at all");
                return;
            }
            MessageBox.Show($"{a}","you typed this");
        }
    }
}
